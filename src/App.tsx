import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  // state (data)
  // On initiatlise un state counter initialiser à 1, et on peut modifier la valeur qu'avec son setter setCounter.
  const [counter, setCounter] = useState(1);

  // comportements
  const handleCount = () => {
    setCounter(counter + 1);
  };

  // render (affichage / rendu)
  return (
    <div>
      <h1>{counter}</h1>
      <button onClick={handleCount} >Add</button>
    </div>
  )
}

export default App;
