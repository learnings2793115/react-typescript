import { FC } from "react";

interface Fruit {
    name: string,
    id: number,
}

interface FruitProps {
    key: number,
    fruitInfo: Fruit,
    onClick: () => void,
}

export default function Fruit(props: FruitProps) {
    // state
    const fruit = props.fruitInfo;
    // comportement
    // render
    return (
        <div>
            <li>
                {fruit.name} <button onClick={props.onClick} >X</button>
            </li>
        </div>
    )

}
