import React, { ChangeEvent, SyntheticEvent, useState } from 'react';
import logo from './logo.svg';
import '../App.css';
import Fruit from "./Fruit"
import Form from "./Form"


function List() {
    interface Fruit {
        id: number;
        name: string;
    }

    // State
    const [fruits, setFruits] = useState([
        { id: 1, name: "Abricot" },
        { id: 2, name: "Banane" },
        { id: 3, name: "Cerise" }
    ]);

    // Events
    const handleDelete = (id: number) => {
        const fruitsCopy = [...fruits];
        const fruitsCopyUpdated = fruitsCopy.filter(
            (fruit) => fruit.id !== id
        );
        setFruits(fruitsCopyUpdated);
    };

    const handleAdd = (addFruit: Fruit) => {
        const fruitsCopy = [...fruits];
        fruitsCopy.push(addFruit); // equivalent to {id: id, name: name}
        setFruits(fruitsCopy);
    };


    // Render
    return (
        <div>
            <h1>Fruits List</h1>
            <ul>
                {
                    fruits.map(
                        (fruit) => (
                            <Fruit
                                fruitInfo={fruit}
                                onClick={() => handleDelete(fruit.id)}
                                key={fruit.id}
                            />
                        )
                    )
                }
            </ul>

            <Form
                handleAdd={handleAdd}
            />

        </div>
    )
}

export default List;