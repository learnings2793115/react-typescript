import { ChangeEvent, FC, SyntheticEvent, useState } from "react";

interface Fruit {
    id: number;
    name: string;
}

interface FormProps {
    handleAdd: (addFruit: Fruit) => void;
}

export default function Form(props: FormProps) {
    // state
    const [newFruit, setNewFruit] = useState<string>();

    // comportements
    const handleSubmit = (event: SyntheticEvent) => {
        event.preventDefault();

        const id = new Date().getTime();
        const name: string = newFruit!;

        props.handleAdd({ id, name })
        setNewFruit(""); // to cean the form chan.
    };

    const handleChangeInput = (event: ChangeEvent<HTMLInputElement>) => {
        setNewFruit(event.target.value);
    };

    // render
    return (
        <form action="submit" onSubmit={handleSubmit} >
            <input
                value={newFruit}
                type="text"
                placeholder="Add fruit"
                onChange={handleChangeInput}
            />
            <button> Add </button>
        </form>
    )
}